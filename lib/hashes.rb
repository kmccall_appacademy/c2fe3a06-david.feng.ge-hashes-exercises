# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = {}
  str.split.each do |word|
    word_hash[word] = word.length
  end

  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |key, value| value }[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |new_key, new_value|
    older[new_key] = new_value
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count_hash = Hash.new(0)
  word.chars.each do |letter| #or use each_char
    letter_count_hash[letter] += 1
  end

  letter_count_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_element = Hash.new(0)
  arr.each do |element|
    uniq_element[element] = 1
  end

  #uniq_element.keys
  uniq_array = []
  uniq_element.each do |element, count|
    uniq_array << element
  end

  uniq_array
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hsh = {even:0, odd:0}
  numbers.each do |number|
    if number.even?
      hsh[:even] += 1
    else
      hsh[:odd] += 1
    end
  end

  hsh
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count_hash = Hash.new(0)
  string.chars.each do |letter|
    vowel_count_hash[letter] += 1 if "aeiou".include?(letter)
  end
  vowel_count_hash.sort_by { |vowel, count| count }[-1].first
end
# vowels = %w(a e i o u)

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  sub_students = students.select { |student, month | month >= 7 && month <= 12 }.keys
  sub_students.combination(2).to_a
end
# names = students.keys
# result = []
#
# names.each_index do |idx1|
#   ((idx1 + 1)...names.length).each do |idx2|
#     result << [ names[idx1], names[idx2] ]
#   end
# end
#
# result

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  population = Hash.new(0)
  specimens.each do |species|
    population[species] += 1
  end
  smallest_population_size = population.sort_by { |species, count| count }[0][1]
  # population.values.min
  largest_population_size =  population.sort_by { |species, count| count }[-1][1]
  population.keys.length ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_characters = character_count(normal_sign.delete("\',.;:!?"))
  vandalized_sign_characters = character_count(vandalized_sign)
  vandalized_sign_characters.all? { |letter, count| normal_sign_characters[letter] >= count }

end

def character_count(str)
  character_counts = Hash.new(0)
  str.downcase.each_char do |letter|
    next if letter == " "
    character_counts[letter] += 1
  end
  character_counts
end
